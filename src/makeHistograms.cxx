#include "generateHistogram.h"
#include "generateHistogram_1lep.h"
#include "ReadTruthTree.h"
#include <vector>
#include <iostream>

// This macro takes ntuples produced using the VHbb truth analysis framework as inputs (with a very loose event selection)
// The macro will then apply:
// * The event selection of your chosen analysis
// * Muon in jet correction (being added)
// * Truth b-jet tagging (considering all jets)
// * 

int main( int argc, char * argv[]  ){

  int sampleToRun = 0;
  if(argc > 1) sampleToRun = (std::atoi(argv[1]));
  TString outputPath = "./";
  if(argc > 2) outputPath = argv[2];
  const std::vector<TString> DSIDs = {"data15","data16","data17","WHlv125J_MINLO","ZH125J_MINLO","ggZH125","ttbar_nonallhad","WenuB","WenuC","WenuL","WenuHPT","WmunuB","WmunuC","WmunuL","WmunuHPT","WtaunuB","WtaunuC","WtaunuL","WtaunuHPT","singletop_t","singletop_s","singletop_Wt","WW","ZZ","WZ","ZeeB","ZeeC", "ZeeL","ZeeHPT", "ZmumuB","ZmumuC", "ZmumuL","ZmumuHPT","ZtautauB","ZtautauC", "ZtautauL","ZtautauHPT","ZnunuB","ZnunuC", "ZnunuL","ZnunuHPT"};
  TString NAME = DSIDs.at(sampleToRun);
  //if(NAME.Contains("singletop_t")) return 0; //For MC16d
  generateHistogram_1lep config;

  //MC16d SR Tag31-10 met/mtw cut removed
  config.SetInputPath("/afs/in2p3.fr/home/y/yzhao/ws/Yanhui/CxAODFramework_master/run/submitDirGENominalOnly-MVA/data-MVATree/Reduce_2Tag_High");
  //MC16d CR Tag31-10 includes both high and medium regions, 1 and 2 tag
  //config.SetInputPath("/afs/in2p3.fr/home/y/yzhao/ws/Yanhui/CxAODFramework_master/run/submitDirGENominalOnly-MVA/data-MVATree/Reduce_MJ");
  
  //MC16a SR Tag31-10 met/mtw cut removed
  //config.SetInputPath("/sps/atlas/y/yma/CxAODFramework_master/run/submitDirGENominalOnly-MVA/data-MVATree/Reduce_2Tag_Medium");
  //MC16a CR Tag31 includes both high and medium regions, 1 and 2 tag
  //config.SetInputPath("/sps/atlas/y/yma/CxAODFramework_master/run/submitDirGENominalOnly-MVA/data-MVATree/Reduce_MJ");
  
  config.SetOutputPath(outputPath); //Set the output path for histograms
  config.SetNEvents(-1);  // Number of events to run over, -1 = all events
  config.SetDebug(false);  // Run in debug mode
  config.SetLumi(36.1);  // Set the luminosity to normalise MC to, in fb
  config.RunMJ(false);//Yanhui
  config.RunCBA(false);//Yanhui
  config.RunMC16a(false);//Yanhui
  config.UseSFs(false);//Yanhui
  config.TightIso(false);//Yanhui
  //Do you aply the tau veto?!!!
  config.GenerateHistogram(generateHistogram::OneLepton,DSIDs.at(sampleToRun));
}

