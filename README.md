Script to produce histograms from ntuples produced by CxAODFramework Reader. The script runs on individual DSIDs

After downloading the scripts from git, you need to setup ROOT6 using
source setup.sh

Then, all that's needed to do is compile the code using make.

The NtupleToHist code is then run by doing ./NtupleToHist 

Additionally, the shell script runNtupleToHist.sh ise included to run the jobs on a PBS batch system.  However, if these are compatible with your univeristy cluster, then feel free to add your own implementation!


# Running the code

The code is steered by makeHistograms.cxx. In here, you can set a number of options:
* config.SetInputPath("/unix/atlascxaods/TruthTuples");  //Set the input path where ntuples are stored
* config.SetOutputPath(outputPath); //Set the output path for histograms
* config.SetNEvents(-1);  // Number of events to run over, -1 = all events
* config.SetDebug(false);  // Run in debug mode

DSepending on how you have set the steering file, after running make, you can launch the script with ./NtupleToHist (where additional arguments set specific options, as defined in makeHistograms.cxx).


Git global setup :

git config --global user.name "Yanhui Ma";
git config --global user.email "yanhui.ma@cern.ch";

Create a new repository :

git clone https://:@gitlab.cern.ch:8443/yama/NtupleReader.git;
cd NtupleReader;
touch README.md;
git add README.md;
git commit -m "add README";
git push -u origin master;

Existing folder 
cd existing_folder 
git init 
git remote add origin https://:@gitlab.cern.ch:8443/yama/NtupleReader.git 
git add . 
git commit -m "Initial commit" 
git push -u origin master

Existing Git repository 
cd existing_repo 
git remote add origin https://:@gitlab.cern.ch:8443/yama/NtupleReader.git 
git push -u origin --all 
git push -u origin --tags
